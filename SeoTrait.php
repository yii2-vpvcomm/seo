<?php
/**
 * Created at: 08.04.2018 17:38
 * @author vpvcomm <vpvcomm@gmail.com>
 * @link http://vpvcomm.ru/
 * @copyright Copyright (c) 2018 vpvcomm
 */

namespace vpvcomm\seo;

/**
 * Universal trait to use inside your
 * Yii2 component and native PHP class
 *
 * @package vpvcomm\seo
 */
trait SeoTrait
{
    protected $title;
    protected $description;
    protected $keywords;
    protected $image;
    protected $url;
    protected $author;
    protected $site;

    protected $section = 'Article';
    protected $type = 'website';
    protected $typeJsonLd = 'WebPage';
    protected $context = 'http://schema.org';
    protected $card = 'summary_large_image';

    public $minify = false;
    public $sizeAnons = 300;
    public $sizeKeywords = 7;

    public $isOpenGraph = true;
    public $isJsonLd = true;
    public $isTwitterCard = true;
    public $isNativeMeta = true;

    public $defaultKeywords = '';
    public $defaultImage = 'http://site.ru/ogimg.jpg';

    /**
     * Advanced Text Clearing
     * @param $str
     * @return string
     */
    public function cleanText($str)
    {
        $replace = strip_tags($str);
        $replace = preg_replace([
            "/[^\\p{L}0-9\\s\\.\\,\\:\\-\\!\\?]/iu",
            "/\r\n/",
            "/\n/",
            "/laquo|raquo|quote/"
        ],"",$replace);
        return trim($replace);
    }

    /**
     * Create an announcement of the desired length
     * @param $str
     * @return bool|string
     */
    public function getAnons($str)
    {
        $clearText = $this->cleanText($str);
        $anons = substr($clearText,0,$this->sizeAnons);
        $anons = trim(substr($anons,0, strrpos($anons, ' ')));
        return $anons;
    }

    /**
     * Minimizing code in one line
     * @param $str
     * @return mixed
     */
    protected function minifyStr($str)
    {
        $str = preg_replace(['/\r\n/','/\n/','/\s{2,}/'],['',' '],$str);
        return $str;
    }

    /**
     * Display all meta micro-layout tags
     * in head section
     * @return mixed|string
     */
    public function metaDisplay()
    {
        try {
            $html = "\n";
            if ($this->isNativeMeta) {
                $html .=<<<HTML
                
<meta name="description" content="{$this->getDescription()}">
<meta name="keywords" content="{$this->getKeywords()}">
HTML;
            }
            if ($this->isOpenGraph) {
                $html .=<<<HTML
                
<meta property="og:type" content="{$this->getType()}">            
<meta property="og:title" content="{$this->getTitle()}">
<meta property="og:description" content="{$this->getDescription()}">
<meta property="og:article:tag" content="{$this->getKeywords()}">
<meta property="og:image" content="{$this->getImage()}">
<meta property="og:url" content="{$this->getUrl()}">
<meta property="og:article:author" content="{$this->getAuthor()}">
<meta property="og:article:section" content="{$this->getSection()}">
HTML;
            }
            if ($this->isJsonLd) {
                $html .=<<<HTML
                
<script type="application/ld+json">
    {
        "@context": "{$this->context}",
        "@type": "{$this->typeJsonLd}",
        "name": "{$this->getTitle()}",
        "description": "{$this->getDescription()}",
        "keywords": "{$this->getKeywords()}",
        "image": "{$this->getImage()}",
        "author": "{$this->getAuthor()}",
        "url": "{$this->getUrl()}"
    }
</script>
HTML;
            }
            if ($this->isTwitterCard) {
                $html .=<<<HTML
                
<meta name="twitter:card" content="{$this->card}">
<meta name="twitter:url" content="{$this->getUrl()}">
<meta name="twitter:title" content="{$this->getTitle()}">
<meta name="twitter:description" content="{$this->getDescription()}">
<meta name="twitter:image:src" content="{$this->getImage()}">
HTML;
            }
            $html .= "\n\n";
            if ($this->minify) {
                $html = $this->minifyStr($html);
            }
            return $html;
        } catch (\Exception $exception) {
            return '';
        }
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Generate a cleaned description
     * @param $description
     */
    public function setDescription($description)
    {
        $this->description = $this->getAnons($description);
    }

    /**
     * @return mixed
     */
    public function getKeywords()
    {
        return trim($this->keywords.','.$this->defaultKeywords,',');
    }

    /**
     * Forming Keywords
     * @param $keywords
     */
    public function setKeywords($keywords)
    {
        $keywords = mb_strtolower($keywords);
        if (strpos($keywords,',') === false) {
            $keywords = $this->cleanText($keywords);
            $keywords = preg_replace(
                ['/\b\w{1,3}\b/u','/\s{2,}/','/\s/','/,,/'],
                ['',' ',',',','],
                $keywords);
            $keywords = trim($keywords,',');
            $arr = explode(',',$keywords);
            $arr = array_slice($arr,0,$this->sizeKeywords);
            $keywords = implode(',',$arr);
        }
        $this->keywords = $keywords;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image ? $this->image : $this->defaultImage;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        $this->url = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
        return $this->url;
    }

    /**
     * @return mixed
     */
    public function getSite()
    {
        $this->site = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].$_SERVER['SERVER_NAME'];
        return $this->site;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author ? $this->author : $_SERVER['SERVER_NAME'];
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return string
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * @param string $section
     */
    public function setSection($section)
    {
        $this->section = $section;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
}