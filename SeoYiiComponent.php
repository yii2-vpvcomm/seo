<?php
/**
 * Created at: 08.04.2018 20:06
 * @author vpvcomm <vpvcomm@gmail.com>
 * @link http://vpvcomm.ru/
 * @copyright Copyright (c) 2018 vpvcomm
 */

namespace vpvcomm\seo;

use Yii;
use yii\base\Component;

/**
 * Yii2 component
 *
 * @package vpvcomm\seo
 */
class SeoYiiComponent extends Component
{
    use SeoTrait;
}
